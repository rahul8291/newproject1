/// <reference types="Cypress" />
import HomePage from '../pageObject/HomePage'
// const { data } = require("cypress/types/jquery")

describe("Assertions", function() {
    before(function() {
        cy.fixture('example').then(function(data) {
            globalThis.data=data
        })
    })
    it("Chai", function() {
       
        cy.visit("https://www.automationteststore.com/");
        cy.get('.info_links_footer > :nth-child(5) > a').click().then(function(linkText) {
           const text=linkText.text() 
            cy.log("This is text "+text)
            Cypress.env('test',text)
            // cy.wrap(text).as('pTest') solution 1
        })

        cy.log("This is testing "+ Cypress.env('test'))
        //solution 1
        // cy.get('@pTest').then(text => {
        //     cy.log(text)
        // })
        // cy.log("This is texting "+test)
        // assert.deepEqual({id:'ContactUsFrm_email'},{id: 'ContactUsFrm_emai'})
        cy.get('#ContactUsFrm_email').should('have.attr', 'name', 'email')
        cy.get('.pull-left > b').should('have.text', 'Address:')
    })

    it("Navigation bar", () => {
        cy.visit("https://www.automationteststore.com/");
        const header=cy.get('#block_frame_featured_1769 > .heading1 > .maintext').then(($headerText) => {
            const headz=$headerText.text();
            cy.log("I found a header "+ headz)
        })
        
    })

    it("Contact us", () => {
        cy.visit("https://automationteststore.com/index.php?rt=content/contact");
        cy.contains('#ContactUsFrm', 'Contact Us Form').find('#field_11').should('contain', 'First name:')
        })

        it("My first project", () => {
            const homePage=new HomePage()
            cy.visit("https://rahulshettyacademy.com/angularpractice/");
            homePage.getEditBox().type('Bob')
            homePage.getTwoWayDataBinding().should('have.value','Bob')
            homePage.getRadioDisabledButton().should("be.disabled")
            homePage.getEditBox().should('have.attr','minlength','2')
            })

            it("My second project", () => {
                 
                cy.visit("https://rahulshettyacademy.com/angularpractice/shop");
                data.productName.forEach(element => {
                    cy.selectProduct(element)
                });
                cy.contains('Checkout').click()
                cy.get('button.btn.btn-success').click()
                cy.get('#country').type('India')
                cy.get('.suggestions > ul > li > a', {timeout:6000}).click()
                cy.get('input[type="checkbox"]').click({force:true})
                cy.get('.ng-untouched > .btn').click()
                cy.get('.alert').invoke('text').then((test) =>{
                    cy.get('.alert').should('have.text',test)
                })

    it.only("My third project", () => {
                 
        cy.visit("https://rahulshettyacademy.com/angularpractice/shop");
        data.productName.forEach(element => {
            cy.selectProduct(element)
                });
                cy.contains('Checkout').click()
                   
                })
                
            })

            
})