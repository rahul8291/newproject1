/// <reference types="Cypress" />

describe("Assertions", function() {
    before(function() {
        cy.fixture('example').then(function(data) {
            globalThis.data=data
        })
    })
    it("Chai", function() {
        var time=6000
        cy.visit(Cypress.env('url'))
        cy.get(data.contact,{timeout: time}).click({force:true})
        cy.get(data.first_name,{timeout: time}).type(Cypress.env('firstName'))
        cy.get(data.emailTest, {timeout: time}).type(Cypress.env('email')) 
        cy.get(data.emailTest, {timeout: time}).should('have.attr','name','email')
        cy.get(data.enquiry, {timeout:time}).type(Cypress.env('enquiry'))
        cy.get(data.submit, {timeout: time}).click({force: true})
        cy.get('.mb40 > :nth-child(3)').should('have.text','Your enquiry has been successfully sent to the store owner!')
    })
    it.only("Chai selector", function() {
        var time=6000
        var timeout=3000
        cy.visit(Cypress.env('url'))
        cy.get("div.fixed_wrapper a[title='Skinsheen Bronzer Stick']").invoke('text').as('texting')
        cy.get('@texting').should('have.text','Skinsheen Bronzer Stick')
    })
})